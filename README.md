# Plasma Mobile Browser

This is an experimental webbrowser designed to 

- be used on small mobile devices,
- integrate well in Plasma workspaces

It is built on top of QtWebEngine, and thus requires Qt 5.4.


Preliminary TODO:
- browser navigation: back + forward + reload (done)
- browser status (done)
- Implement URL bar (done)
- Error handler in UI (done)
- in-window navigation: tabs vs. top bar
- SSL error handler
- Touch actions (pinch?)
- user-agent to request mobile site (done)
- adblock
- kwallet integration

In progress:
- bookmarks store (done)
- bookmarks model (done)
- bookmarks UI
  - show bookmarks (done)
  - add /  remove
